<?php
defined('BASEPATH') or exit('No direct script access allowed');

class MyModel extends CI_Model
{

    public function Ambil_semua_data_mahasiswa()
    {
        $query = "SELECT tb_mahasiswa.nim,tb_mahasiswa.nama_mahasiswa,tb_mahasiswa.tempat_lahir,
        tb_mahasiswa.tanggal_lahir,tb_mahasiswa.alamat,tb_mahasiswa.tahun_masuk,tb_mahasiswa.kelas,
        tb_mahasiswa.image,tb_prodi.nama_prodi
        FROM tb_mahasiswa JOIN tb_prodi
        ON tb_mahasiswa.kd_prodi = tb_prodi.kd_prodi
        ORDER BY tb_mahasiswa.nim ASC";
        return $this->db->query($query)->result_array();
    }

    public function Ambil_data_prodi()
    {
        return $query = $this->db->get('tb_prodi')->result_array();
        // keterangan: 
        // - query diatas = "SELECT * FROM tb_prodi" 
        // - kita gunakan query builder dari CodeIgniter untuk memanggil semua data pada tabel tb_prodi
        // - data yang di ambil disimpan dalam bentuk array 
    }

    public function Ambil_data_mahasiswa_by_nim($nim)
    {
        $this->db->select('*');
        $this->db->from('tb_mahasiswa');
        $this->db->join('tb_prodi', 'tb_mahasiswa.kd_prodi = tb_prodi.kd_prodi');
        $this->db->where(array('nim' => $nim));
        return $query = $this->db->get()->row_array();
        // keterangan: 
        // - query diatas sama seperti query pada fungsi Ambil_semua_data_mahasiswa(), namun dengan menggunakan klausa WHERE
        // - wuery diatas menggunakan query builder dari CodeIgniter untuk memanggil data mahassiswa pada tabel tb_mahasiswa, dan nama prodipada tb_prodi
        // - data yang di ambil disimpan dalam bentuk array 
    }

    public function hapus_data_mahasiswa($nim)
    {
        $this->db->where('nim', $nim);
        $this->db->delete('tb_mahasiswa');
    }
}
