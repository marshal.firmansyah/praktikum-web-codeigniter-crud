<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Form Register</title>
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/bootstrap/css/bootstrap.min.css">
</head>

<body>
    <section>
        <div class="container h-100">
            <div class="row justify-content-sm-center h-100">
                <div class="col-xxl-4 col-xl-5 col-lg-5 col-md-7 col-sm-9">
                    <div class="text-center my-5">
                        <img src="<?= base_url() ?>assets/image/logo.png" alt="logo" width="100">
                    </div>
                    <div class="card shadow-lg">
                        <div class="card-body p-5">
                            <h2 class="fs-4 card-title fw-bold mb-4">Register</h2>
                            <form class="form-horizontal" action="<?php echo base_url('register/save'); ?>" method="post" enctype="multipart/form-data">
                                <div class="mb-3">
                                    <label class="mb-2 text-muted" for="nama">Nama Lengkap</label>
                                    <input type="text" class="form-control" placeholder="Nama Lengkap" name="nama" id="nama" placeholder="nama" required oninvalid="this.setCustomValidity('Data tidak boleh kosong')" oninput="setCustomValidity('')">
                                </div>

                                <div class="mb-3">
                                    <label class="mb-2 text-muted" for="username">Username</label>
                                    <input type="text" class="form-control" name="username" id="username" placeholder="Username" required oninvalid="this.setCustomValidity('Data tidak boleh kosong')" oninput="setCustomValidity('')"></input>
                                </div>

                                <div class="mb-3">
                                    <label class="mb-2 text-muted" for="password">Password</label>
                                    <input type="password" class="form-control" name="password" id="password" placeholder="Password" required oninvalid="this.setCustomValidity('Data tidak boleh kosong')" oninput="setCustomValidity('')"></input>
                                </div>

                                <div class="mb-3">
                                    <label class="mb-2 text-muted" for="email">Email</label>
                                    <input type="email" class="form-control" name="email" id="email" placeholder="Email" required oninvalid="this.setCustomValidity('Data tidak boleh kosong')" oninput="setCustomValidity('')"></input>
                                </div>

                                <div class="form-group">
                                    <label class="mb-2 text-muted" for="pilih_level">Pilih Level</label>
                                    <select class="form-control" style="width: 100%;" name="level_id">
                                        <option value="">-- Pilih Level --</option>
                                        <?php
                                        foreach ($get_level as $data) { // Diambil dari controller
                                            echo "<option value='" . $data->level_id . "'>" . $data->nama . "</option>";
                                        }
                                        ?>
                                    </select>
                                </div>

                                <div class="row">
                                    <div class="col-4">
                                        <button type="submit" class="btn btn-primary btn-block">Register</button>
                                    </div>
                                </div>

                            </form>
                        </div>
                        <div class="card-footer py-3 border-0">
                            <div class="text-center">
                                Sudah Punya Akun? <a href="<?php echo base_url('login') ?>" class="text-dark">Login</a>
                            </div>
                        </div>
                    </div>
                    <div class="text-center mt-5 text-muted">
                        Copyright &copy; 2022 &mdash; Institut Teknologi Garut
                    </div>
                </div>
            </div>
        </div>
    </section>
</body>

</html>