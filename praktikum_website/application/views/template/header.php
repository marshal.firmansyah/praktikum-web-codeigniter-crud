<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1,shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Dashboard Mahasiswa</title>

    <!-- Custom fonts for this template-->
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

    <!-- Vendor CSS Files -->
    <link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/data_tables/dataTables.bootstrap4.min.css">


</head>

<body>
    <nav class="navbar navbar-expand-lg navbar-dark bg-primary text-left shadow">
        <div class="container-fluid">
            <a class="navbar-brand" href="#" style="padding-left: 5%;">
                <img src="<?= base_url('assets/image/logo.png'); ?>" alt="" width="50" class="d-inline-block align-text-center">
                <b style="margin-left: 10px;">Belajar CRUD di CodeIgniter</b></a>
        </div>
        <a href="<?php echo base_url('login/logout'); ?>" class="nav-link">
                    <i class="nav-icon far fa-window-close"></i>
                    <button type="button" class="btn btn-danger">Logout</button>
                </a>
    </nav>