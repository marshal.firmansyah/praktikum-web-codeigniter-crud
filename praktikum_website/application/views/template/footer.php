<!-- Footer -->

<footer class="sticky-footer bg-white shadow">
    <div class="container">
        <div class="copyright text-center p-4" style="margin: auto;">
            &copy; Copyright <?= date('Y'); ?> <strong><span>Institut Teknologi Garut</span></strong>
        </div>
    </div>
</footer>

<script type="text/javascript" src="<?= base_url() ?>assets/bootstrap/js/bootstrap.bundle.min.js"> </script>
<script type="text/javascript" src="<?= base_url() ?>assets/data_tables/jquery-3.3.1.min.js"></script>
<script type="text/javascript" src="<?= base_url() ?>assets/data_tables/bootstrap.min.js"></script>
<script type="text/javascript" src="<?= base_url() ?>assets/data_tables/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="<?= base_url() ?>assets/data_tables/dataTables.bootstrap4.min.js"></script>

<script type="text/javascript">
    $(document).ready(function() {
        $('#table_data').DataTable();
    });
</script>



</body>

</html>