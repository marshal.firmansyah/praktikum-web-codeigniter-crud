<div class="container mb-5">
    <h4 class="my-5">Halaman Pengelolaan Data Mahasiswa</h4>
    <div class="table-responsive">
        <hr>
        <table class="table table-striped table-hover display shadow" id="table_data">
            <thead>
                <tr class="text-center">
                    <th scope="col">No</th>
                    <th scope="col">Nama</th>
                    <th scope="col">TTL</th>
                    <th scope="col">Alamat</th>
                    <th scope="col">kelas</th>
                    <th scope="col">angkatan</th>
                    <th scope="col">prodi</th>
                    <th scope="col">foto</th>
                </tr>
            </thead>
            <tbody>
                <?php $i = 1;
                foreach ($mahasiswa as $mhs) { ?>
                    <tr class="text-center">
                        <th><?= $i; ?></th>
                        <td style="text-align: left;"><?= $mhs['nama_mahasiswa']; ?></td>
                        <td><?= $mhs['tempat_lahir'] . ", " . $mhs['tanggal_lahir']; ?></td>
                        <td><?= $mhs['alamat']; ?></td>
                        <td><?= $mhs['kelas']; ?></td>
                        <td><?= $mhs['tahun_masuk']; ?></td>
                        <td><?= $mhs['nama_prodi'] ?></td>
                        <td><img src="<?= base_url('assets/image/') . $mhs['image']; ?>" style="width: 5vw;" alt="..."></td>
                    </tr>
                <?php $i++;
                } ?>
            </tbody>
        </table>
    </div>
</div>
