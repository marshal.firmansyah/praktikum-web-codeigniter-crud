<div class="container mb-5">
    <h4 class="my-5">Formulir edit data mahasiswa</h4>
    <?= $this->session->flashdata('message'); ?>
    <form class="form-control shadow" method="post" action="<?= base_url('Admin/perbaharui_data/' . $mahasiswa['nim']); ?>" enctype="multipart/form-data">
        <div class="row p-3">
            <div class="col-sm-6">
                <div class="form-group">
                    <label for="nim" class="form-label">Nim</label>
                    <input type="text" class="form-control text-secondary" id="nim" name="nim" value="<?= $mahasiswa['nim']; ?>" disabled>
                </div>
                <div class="form-group">
                    <label for="nama" class="form-label mt-2">Nama Lengkap</label>
                    <input type="text" class="form-control text-secondary" id="nama" name="nama" value="<?= $mahasiswa['nama_mahasiswa']; ?>">
                </div>
                <div class="form-group">
                    <label for="tmp_lahir" class="form-label mt-2">Tempat Lahir</label>
                    <input type="text" class="form-control text-secondary" id="tmp_lahir" name="tmp_lahir" value="<?= $mahasiswa['tempat_lahir']; ?>">
                </div>
                <div class="form-group">
                    <label for="tgl_lahir" class="form-label mt-2">Tanggal Lahir</label>
                    <input type="date" class="form-control text-secondary" id="tgl_lahir" name="tgl_lahir" value="<?= $mahasiswa['tanggal_lahir']; ?>">
                </div>
                <div class="form-group">
                    <label for="alamat" class="form-label mt-2">Alamat</label>
                    <textarea class="form-control text-secondary" id="alamat" name="alamat" value="<?= $mahasiswa['alamat']; ?>"><?= $mahasiswa['alamat']; ?></textarea>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="form-group">
                    <label for="prodi" class="form-label">Prodi</label>
                    <select class="form-control text-secondary" id="prodi" name="prodi">
                        <option selected disabled value="">--Pilih Prodi--</option>
                        <?php foreach ($prodi as $prd) { ?>
                            <option value="<?= $prd['kd_prodi']; ?>" <?php if ($mahasiswa['kd_prodi'] == $prd['kd_prodi']) {
                                    echo "selected";
                                    } ?>>
                                <?= $prd['nama_prodi']; ?>
                            </option>
                        <?php } ?>
                    </select>
                </div>
                <div class="form-group">
                    <label for="thn_masuk" class="form-label mt-2">Tahun Masuk</label>
                    <input type="number" class="form-control text-secondary" id="thn_masuk" name="thn_masuk" value="<?= $mahasiswa['tahun_masuk']; ?>">
                </div>
                <div class="form-group">
                    <label for="kelas" class="form-label mt-2">Kelas</label>
                    <input type="text" class="form-control text-secondary " id="kelas" name="kelas" value="<?= $mahasiswa['kelas']; ?>">
                </div>
                <div class="form-group">
                    <label for="prodi" class="form-label mt-2">Foto</label>
                    <div class="row">
                        <div class="col-md-3">
                            <img src="<?= base_url('assets/image/') . $mahasiswa['image']; ?>" style="width: 8vw;" alt="...">
                        </div>
                        <div class="col-md-9">
                            <input type="file" class="form-control" id="foto" name="foto" accept=".png,.jpeg,.jpg" arialabel="fileexample">
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <hr class="mt-3">
        <button class="btn btn-primary mx-3 mb-3" type="submit">Perbaharui</button>
    </form>
</div>