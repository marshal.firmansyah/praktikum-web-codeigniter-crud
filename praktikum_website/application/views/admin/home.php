<div class="container mb-5">
    <h4 class="my-5">Halaman Pengelolaan Data Mahasiswa</h4>
    <?= $this->session->flashdata('message'); ?>
    <a href="<?= base_url('Admin/tambah_data'); ?>" type="button" class="btn btn-sm btn-primary" style="margin-left:25px">Tambah Data</a>
    <div class="table-responsive">
        <hr>
        <table class="table table-striped table-hover display shadow" id="table_data">
            <thead>
                <tr class="text-center">
                    <th scope="col">No</th>
                    <th scope="col">Nama</th>
                    <th scope="col">TTL</th>
                    <th scope="col">Alamat</th>
                    <th scope="col">kelas</th>
                    <th scope="col">angkatan</th>
                    <th scope="col">prodi</th>
                    <th scope="col">foto</th>
                    <th scope="col">Aksi</th>
                </tr>
            </thead>
            <tbody>
                <?php $i = 1;
                foreach ($mahasiswa as $mhs) { ?>
                    <tr class="text-center">
                        <th><?= $i; ?></th>
                        <td style="text-align: left;"><?= $mhs['nama_mahasiswa']; ?></td>
                        <td><?= $mhs['tempat_lahir'] . ", " . $mhs['tanggal_lahir']; ?></td>
                        <td><?= $mhs['alamat']; ?></td>
                        <td><?= $mhs['kelas']; ?></td>
                        <td><?= $mhs['tahun_masuk']; ?></td>
                        <td><?= $mhs['nama_prodi'] ?></td>
                        <td><img src="<?= base_url('assets/image/') . $mhs['image']; ?>" style="width: 5vw;" alt="..."></td>
                        <td scope="col">
                            <a href="<?= base_url('Admin/ubah_data/') . $mhs['nim']; ?>" class="btn btn-sm btn-warning">Ubah</a>
                            <button type="button" class="btn btn-sm btn-danger" data-toggle="modal" data-target="#exampleModal">Hapus</button>
                        </td>
                    </tr>
                <?php $i++;
                } ?>
            </tbody>
        </table>
    </div>
</div>

<!-- Modal -->
<?php foreach ($mahasiswa as $mhs) { ?>
    <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Hapus Data</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    Yakin akan menghapus data?
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-sm btn-secondary" data-dismiss="modal">Batal</button>
                    <a href="<?= base_url('Admin/hapus/') . $mhs['nim']; ?>" type="button" class="btn btn-danger btn-sm">Hapus</a>
                </div>
            </div>
        </div>
    </div>
<?php } ?>