<div class="container mb-5">
    <h4 class="my-5">Formulir pengisian data mahasiswa</h4>
    <?= $this->session->flashdata('message'); ?>
    <form class="form-control shadow" method="post" action="<?= base_url('Admin/tambah_data'); ?>" enctype="multipart/form-data">
        <div class="row p-3">
            <div class="col-sm-6">
                <div class="form-group">
                    <label for="nim" class="form-label">Nim</label>
                    <input type="text" class="form-control text-secondary" id="nim" name="nim" value="<?= set_value('nim'); ?>">
                    <?= form_error('nim', '<small class="text-danger pl-3">', '</small>'); ?>
                </div>

                <div class="form-group">
                    <label for="nama" class="form-label mt-2">Nama Lengkap</label>
                    <input type="text" class="form-control text-secondary" id="nama" name="nama" value="<?= set_value('nama'); ?>">
                    <?= form_error('nama', '<small class="text-danger pl-3">', '</small>'); ?>
                </div>

                <div class="form-group">
                    <label for="tmp_lahir" class="form-label mt-2">Tempat Lahir</label>
                    <input type="text" class="form-control text-secondary" id="tmp_lahir" name="tmp_lahir" value="<?= set_value('tmp_lahir'); ?>">
                    <?= form_error('tmp_lahir', '<small class="text-danger pl-3">', '</small>'); ?>
                </div>

                <div class="form-group">
                    <label for="tgl_lahir" class="form-label mt-2">Tanggal Lahir</label>
                    <input type="date" class="form-control text-secondary" id="tgl_lahir" name="tgl_lahir" value="<?= set_value('tgl_lahir'); ?>">
                    <?= form_error('tgl_lahir', '<small class="text-danger pl-3">', '</small>'); ?>
                </div>

                <div class="form-group">
                    <label for="alamat" class="form-label mt-2">Alamat</label>
                    <textarea class="form-control text-secondary" id="alamat" name="alamat" value="<?= set_value('alamat'); ?>"></textarea>
                    <?= form_error('alamat', '<small class="text-danger pl-3">', '</small>'); ?>
                </div>
            </div>

            <div class="col-sm-6">
                <div class="form-group">
                    <label for="prodi" class="form-label">Prodi</label>
                    <select class="form-control text-secondary" id="prodi" name="prodi">
                        <option selected disabled value="">--Pilih Prodi--</option>
                        <?php foreach ($prodi as $prd) { ?>
                            <option value="<?= $prd['kd_prodi']; ?>"><?= $prd['nama_prodi']; ?></option>
                        <?php } ?>
                    </select>
                    <?= form_error('prodi', '<small class="text-danger pl-3">', '</small>'); ?>
                </div>

                <div class="form-group">
                    <label for="thn_masuk" class="form-label mt-2">Tahun Masuk</label>
                    <input type="number" class="form-control text-secondary" id="thn_masuk" name="thn_masuk" value="<?= set_value('thn_masuk'); ?>">
                    <?= form_error('thn_masuk', '<small class="text-danger pl-3">', '</small>'); ?>
                </div>

                <div class="form-group">
                    <label for="kelas" class="form-label mt-2">Kelas</label>
                    <input type="text" class="form-control text-secondary" id="kelas" name="kelas" value="<?= set_value('kelas'); ?>">
                    <?= form_error('kelas', '<small class="text-danger pl-3">', '</small>'); ?>
                </div>

                <div class="form-group">
                    <label for="prodi" class="form-label mt-2">Foto</label>
                    <input type="file" class="form-control text-secondary" id="foto" name="foto" accept=".png,.jpeg,.jpg" arialabel="fileexample">
                </div>
            </div>
        </div>
        <hr class="mt-3">
        <button class="btn btn-primary mx-3 mb-3" type="submit">Simpan</button>
    </form>

</div>