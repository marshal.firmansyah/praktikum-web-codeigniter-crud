<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Admin extends CI_Controller
{

    public function index()
    {
        // membuat title
        $data['title'] = 'Halaman Admin';

        // load model MyModel
        $this->load->model('MyModel');

        if ($this->session->userdata('logged_in') != "" && $this->session->userdata('level_id') == "1") {

            // panggil fungsi ambil_semua_data mahasiswa pada MyModel
            $data['mahasiswa'] = $this->MyModel->Ambil_semua_data_mahasiswa();

            // load view
            $this->load->view('template/header', $data);
            $this->load->view('admin/home', $data);
            $this->load->view('template/footer');
        } else {

            redirect('login/logout');
        }
    }

    public function tambah_data()
    {
        // load model MyModel 
        $this->load->Model('MyModel');

        // aturan tambahan pada kolom input untuk validasi 
        $this->form_validation->set_rules(
            'nim',
            'Nim',
            'required|trim|is_unique[tb_mahasiswa.nim]',
            [
                'required' => 'Data belum di isi!', 'is_unique' => 'Nim Sudah terdaftar'
            ]
        );

        $this->form_validation->set_rules(
            'nama',
            'Nama',
            'required|trim',
            ['required' => 'Nama belum di isi!']
        );

        $this->form_validation->set_rules(
            'tmp_lahir',
            'Tempat Lahir',
            'required|trim',
            ['required' => 'Tempat lahir belum di isi!']
        );

        $this->form_validation->set_rules(
            'tgl_lahir',
            'Tanggal lahir',
            'required|trim',
            ['required' => 'Tanggal lahir belum di isi!']
        );

        $this->form_validation->set_rules(
            'alamat',
            'Alamat',
            'required|trim',
            ['required' => 'Alamat belum di isi!']
        );

        $this->form_validation->set_rules(
            'prodi',
            'Prodi',
            'required|trim',
            ['required' => 'Prodi belum di tentukan!']
        );

        $this->form_validation->set_rules(
            'thn_masuk',
            'Tahun masuk',
            'required|trim',
            ['required' => 'Tahun masuk belum di isi!']
        );

        $this->form_validation->set_rules(
            'kelas',
            'Kelas',
            'required|trim',
            ['required' => 'Kelas belum di isi!']
        );

        // jika form tidak lolos validasi 
        if ($this->form_validation->run() == false) {

            // membuat title 
            $data['title'] = 'Halaman Utama';

            // load model MyModel 
            $this->load->Model('MyModel');

            // panggil fungsi Ambil_data_prodi() pada model "MyModel", lalu simpan pada variabel $data['prodi'] 
            $data['prodi'] = $this->MyModel->Ambil_data_prodi();

            // load view 
            $this->load->view('template/header', $data);
            $this->load->view('admin/form_insert_data', $data);
            $this->load->view('template/footer');
        }

        // jika form lolos validasi 
        else {
            // buat variabel untuk manampung data inputan 
            $nim          = htmlspecialchars($this->input->post('nim', true));
            $nama         = htmlspecialchars($this->input->post('nama', true));
            $tmp_lahir    = htmlspecialchars($this->input->post('tmp_lahir', true));
            $tgl_lahir    = htmlspecialchars($this->input->post('tgl_lahir', true));
            $alamat       = htmlspecialchars($this->input->post('alamat', true));
            $prodi        = htmlspecialchars($this->input->post('prodi', true));
            $thn_masuk    = htmlspecialchars($this->input->post('thn_masuk', true));
            $kelas        = htmlspecialchars($this->input->post('kelas', true));
            $upload_image = $_FILES['foto']['name'];
            $tipe_file    = pathinfo($upload_image, PATHINFO_EXTENSION);

            // jika ada image yang diupload 
            if ($upload_image) {
                $config['allowed_types'] = 'jpg|png|jpeg';
                $config['max_size']     = '2048';
                $config['upload_path'] = './assets/image/';
                $config['file_name'] = 'foto' . '_' . $nim . '.' . $tipe_file;
                $this->load->library('upload', $config);
                if ($this->upload->do_upload('foto')) {
                    $image = $config['file_name'];
                }
            } else {
                $image = 'default_image.jpg';
            }

            // buat data dalam bentuk array yang nanti akan di masukkan ke database
            $data = [
                'nim'             => $nim,
                'nama_mahasiswa'  => $nama,
                'tempat_lahir'    => $tmp_lahir,
                'tanggal_lahir'   => $tgl_lahir,
                'alamat'          => $alamat,
                'tahun_masuk'     => $thn_masuk,
                'kelas'           => $kelas,
                'image'           => $image,
                'kd_prodi'        => $prodi
            ];

            // query builder untuk insert data ke tb_mahasiswa 
            $this->db->insert('tb_mahasiswa', $data);

            // buat notifikasi bahwa data berhasil di tambahkan 
            $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">Data berhasil di tambahkan! </div>');

            // jika sudah, redirect ke halaman utama 
            redirect('admin');
        }
    }

    public function ubah_data($id)
    {
        // membuat title 
        $data['title'] = 'Halaman Utama';

        // load model MyModel 
        $this->load->Model('MyModel');

        // panggil fungsi Ambil_data_prodi() pada model "MyModel", lalu simpan pada variabel $data['prodi']
        $data['prodi'] = $this->MyModel->Ambil_data_prodi();
        $nim = $id;
        $data['mahasiswa'] = $this->MyModel->Ambil_data_mahasiswa_by_nim($nim);

        // load view 
        $this->load->view('template/header', $data);
        $this->load->view('admin/form_ubah_data', $data);
        $this->load->view('template/footer');
    }

    public function perbaharui_data($nim)
    {
        // load model MyModel 
        $this->load->Model('MyModel');
        $data['mahasiswa'] = $this->MyModel->Ambil_data_mahasiswa_by_nim($nim);

        // aturan tambahan pada kolom input untuk validasi 
        $this->form_validation->set_rules('nama', 'Nama', 'required|trim', ['required' => 'Nama belum di isi!']);
        $this->form_validation->set_rules('tmp_lahir', 'Tempat Lahir', 'required|trim', ['required' => 'Tempat lahir belum di isi!']);
        $this->form_validation->set_rules('tgl_lahir', 'Tanggal lahir', 'required|trim', ['required' => 'Tanggal lahir belum di isi!']);
        $this->form_validation->set_rules('alamat', 'Alamat', 'required|trim', ['required' => 'Alamat belum di isi!']);
        $this->form_validation->set_rules('prodi', 'Prodi', 'required|trim', ['required' => 'Prodi belum di tentukan!']);
        $this->form_validation->set_rules('thn_masuk', 'Tahun masuk', 'required|trim', ['required' => 'Tahun masuk belum di isi!']);
        $this->form_validation->set_rules('kelas', 'Kelas', 'required|trim', ['required' => 'Kelas belum di isi!']);

        // jika form tidak lolos validasi 
        if ($this->form_validation->run() == false) {
            // buat notifikasi bahwa data gagal di perbaharui 
            $this->session->set_flashdata('message', '<div class="alert alert-warning" role="alert">Data gagal di perbaharui! </div>');

            // redirect/pindahkan ke halaman lain 
            redirect('admin/ubah_data/' . $nim);
        }

        // jika form lolos validasi 
        else {

            // buat variabel untuk menampung inputan 
            $nama      = htmlspecialchars($this->input->post('nama', true));
            $tmp_lahir = htmlspecialchars($this->input->post('tmp_lahir', true));
            $tgl_lahir = htmlspecialchars($this->input->post('tgl_lahir', true));
            $alamat    = htmlspecialchars($this->input->post('alamat', true));
            $prodi     = htmlspecialchars($this->input->post('prodi', true));
            $thn_masuk = htmlspecialchars($this->input->post('thn_masuk', true));
            $kelas     = htmlspecialchars($this->input->post('kelas', true));

            // cek jika ada gambar yang akan di upload 
            $upload_image = $_FILES['foto']['name'];
            $tipe_file = pathinfo($upload_image, PATHINFO_EXTENSION);
            if ($upload_image) {
                $config['allowed_types'] = 'gif|jpg|png|jpeg';
                $config['max_size']      = '2048';
                $config['upload_path']   = './assets/image/';
                $config['file_name']     = 'foto' . '_' . $nim . '.' . $tipe_file;
                $this->load->library('upload', $config);
                $old_image = $data['mahasiswa']['image'];
                if ($old_image != 'default_image.jpg') {
                    unlink(FCPATH . 'assets/image/' . $old_image);
                    if ($this->upload->do_upload('foto')) {
                        $new_image = $config['file_name'];
                    } else {
                        echo $this->upload->display_errors();
                    }
                }
            }
            // buat variabel untuk manampung data 
            $data2 = array(
                'nim'             => $nim,
                'nama_mahasiswa'  => $nama,
                'tempat_lahir'    => $tmp_lahir,
                'tanggal_lahir'   => $tgl_lahir,
                'alamat'          => $alamat,
                'tahun_masuk'     => $thn_masuk,
                'kelas'           => $kelas,
                'image'           => $new_image, 'kd_prodi' => $prodi
            );

            // print_r($data2); die(); 
            $where = array('nim' => $nim);
            $this->db->where($where);
            $this->db->update('tb_mahasiswa', $data2);
            $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">Data mahasiswa telah di perbaharui!</div>');
            redirect('admin');
        }
    }

    public function hapus($nim)
    {
        // load dulu nama model yang akan digunakan 
        $this->load->model('MyModel');
        $data['mahasiswa'] = $this->MyModel->Ambil_data_mahasiswa_by_nim($nim);

        // hapus dulu image yang tersimpan di sistem 
        unlink(FCPATH . 'assets/image/' . $data['mahasiswa']['image']);

        // kirimkan data ke model untuk penghapusan data di database 
        $this->MyModel->hapus_data_mahasiswa($nim);

        // jika sudah selesai menghapus  maka 
        $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">Data mahasiswa telah berhasil di hapus!</div>');
        redirect('admin');
    }
}
