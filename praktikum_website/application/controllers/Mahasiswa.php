<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Mahasiswa extends CI_Controller
{

    public function index()
    {
        if ($this->session->userdata('logged_in') != "" && $this->session->userdata('level_id') == "2") {

        // membuat title
        $data['title'] = 'Halaman Mahasiswa';

        // load model MyModel
        $this->load->model('MyModel');
        
        // panggil fungsi ambil_semua_data mahasiswa pada MyModel
        $data['mahasiswa'] = $this->MyModel->Ambil_semua_data_mahasiswa();

        // load view
        $this->load->view('template/header', $data);
        $this->load->view('mahasiswa/home', $data);
        $this->load->view('template/footer');
        } else {

            redirect('login/logout');
        }
    }
}
